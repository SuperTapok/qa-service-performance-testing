package consts

trait Common {
  val baseUrl: String                      = "http://localhost:8081"
  val basicId: Int                         =  1000
  val percentOfFailedRequestThreshold: Int =    50
  val responseTimeThreshold: Int           = 30000
}
