package qa_service

import io.gatling.core.Predef._
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import consts.Common

class Scenarios extends Simulation with Common {

  val getAnswersScn: ScenarioBuilder = scenario("Поиск ответа на интересующий вопрос")
    .exec(getQuestions())
    .pause(5)
    .exec(getAnswers())

  val createQuestionScn: ScenarioBuilder = scenario("Создать свой вопрос и ожидать ответ")
    .exec(createQuestion())
    .pause(5)
    .exec(getAnswers())

  val answerQuestionScn: ScenarioBuilder = scenario("Ответить на имеющийся вопрос")
    .exec(getQuestions())
    .pause(5)
    .exec(answerQuestion())

  def getAnswers(): ChainBuilder = {
    doIfOrElse(session => session.contains("id"))(
      http("Проверить ответы на вопрос")
        .get("/questions/#{id}/answers")
    )(
      http("Проверить ответы на вопрос")
        .get("/questions/" + basicId + "/answers"))
  }

  def getQuestions(): ChainBuilder = {
    exec(
      http("Получить все вопросы")
        .get("/questions"))
  }

  def createQuestion(): ChainBuilder = {
    exec(
      http("Создать свой вопрос")
        .post("/questions")
        .body(ElFileBody("question.json"))
        .check(jmesPath("id")
          .saveAs("id")))
  }

  def answerQuestion(): ChainBuilder = {
    exec(
      http("Ответить на вопрос")
        .post("/questions/" + basicId + "/answers")
        .body(ElFileBody("answer.json")))
  }
}
