package qa_service.v1

import consts.Common
import io.gatling.core.Predef._
import io.gatling.core.controller.inject.open.OpenInjectionStep
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import qa_service.Scenarios

class StressSimulation extends Scenarios with Common {
  val httpConf: HttpProtocolBuilder = http.baseUrl(baseUrl)
    .header("content-type", "application/json")

  val loadingModel: OpenInjectionStep  =
    stressPeakUsers(800).during(60)

  setUp(
    createQuestionScn.inject(loadingModel).protocols(httpConf),
    getAnswersScn.inject(loadingModel).protocols(httpConf),
    answerQuestionScn.inject(loadingModel).protocols(httpConf)
  ).assertions(
    global.responseTime.percentile1.lt(responseTimeThreshold),
    global.failedRequests.percent.lt(percentOfFailedRequestThreshold),
  )
}
