package qa_service.v1

import consts.Common
import io.gatling.core.Predef._
import io.gatling.core.controller.inject.closed.ClosedInjectionStep
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import qa_service.Scenarios

class StabilitySimulation extends Scenarios with Common {
  val httpConf: HttpProtocolBuilder = http.baseUrl(baseUrl)
    .header("content-type", "application/json")

  // при 425 - адекватная работа без перебоев, но перцентили не соблюдает
  val loadingModel: List[ClosedInjectionStep]  = List(
    rampConcurrentUsers(0).to(275).during(30),
    constantConcurrentUsers(275).during(120)
  )

  setUp(
    createQuestionScn.inject(loadingModel).protocols(httpConf),
    getAnswersScn.inject(loadingModel).protocols(httpConf),
    answerQuestionScn.inject(loadingModel).protocols(httpConf)
  ).assertions(
    global.responseTime.percentile1.lt(responseTimeThreshold),
    global.failedRequests.percent.lt(percentOfFailedRequestThreshold),
  )
}
